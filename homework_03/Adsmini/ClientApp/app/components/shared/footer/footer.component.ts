﻿import { Component } from '@angular/core';

@Component({
    selector: 'footer-component',
    templateUrl: './footer.component.html'
})
export class FooterComponent {
    title = "footer";
    author = "Усанов Антон";
    year = 2019;
    link = "https://gitlab.com/ant9n19/pps/tree/master/homework_02";
    content = "Все права защищены.";
}