#Lab 2 Patterns

1) lab2(1) - Factory Method C# Example;
2) lab2(2) - Builder C# Example;
3) lab2(3) - Bridge C# Example;
4) lab2(4) - Decorator C# Example;
5) lab2(5) - Command C# Example;
6) lab2(6) - Template Method C# Example;